#!/bin/sh

. bootstrap.sh

log_info "Start script"


add_user() {
    local user=$1
    if [ ! -d /home/$user ]; then
        adduser -g "$user" $user -s /bin/zsh
        adduser $user $g
    fi
    cp skel/zshrc /home/$user/.zshrc
}


log_info "Install base packages"
#install_packages packages-base.txt

log_info "Install extended packages"
#install_packages packages-extended.txt

log_info "Add user"
#add_user test1


exit 0

# vim: sw=4:et:ai
