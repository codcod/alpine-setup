#!/bin/sh

. bootstrap.sh

log_info "Start script"

log_info "Install packages"
#apk --quiet add fd ripgrep nerd-fonts

log_info "Clone repo"
#git clone https://github.com/kabinspace/AstroVim ~/.config/nvim

# TODO: configure or print steps for user to do

recompile_file() {
    local _dir=".local/share/nvim/site/pack/packer/start/telescope-fzf-native.vim"
    local _pwd=$PWD
    if [ -d "$_dir" ]; then
        cd $_dir
        make clean && make
        cd $_pwd
    fi
}

log_info "Recompile file (workaround)"
#recompile_file

exit 0

# vim: sw=4:et:ai
