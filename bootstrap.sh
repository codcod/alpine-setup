# Bootstrap.sh: Offers set of useful functions. To be included in other scipts.

PROGRAM=$( basename $0 )

# Workaround for the situation in which /bin/sh is in fact a symlink to
# BusyBox's sh. This version of sh requires "-e" for "echo" to interpret
# ansi escape codes (not posix compliant in this regard).
[ "$(readlink /bin/sh)" = "/bin/busybox" ] && ECHOE="-e"


#_log() {
#    if [ -z "$QUIET" ]; then
#        local fg_red='\033[0;31m'
#        local fg_blue='\033[0;34m'
#        local fg_norm='\033[0m'
#        local level=$1
#        local color=$2
#        case $color in
#            "red") fg=$fg_red;;
#            "blue") fg=$fg_blue;;
#        esac
#        local pref="$fg$level:$PROGRAM:$fg_norm"
#        shift 2
#        echo $ECHOE "$pref$@"
#    fi
#}

# "Private" function to help constructing log entry.
# Log entry contains indication of a level and the name of the program.
#
# Example entry:
# INFO:test.sh:Hello World
# WARN:test.sh:Possible mistake
# ERROR:test.sh:Definitely a mistake!
#
# Args:
#    $1: "log level", eg. "INFO", "WARN", "ERROR"
#    $2: color of the log entry's prefix, eg. "red", "blue" (well, "default")
_log() {
    local level=$1
    local fg="\033[2m"               # default foreground: dimmed
    [ "red" = "$2" ] && fg="\033[0;31m" # if indicated, then: red
    shift 2
    # finally, print the log entry starting with a colored prefix
    echo $ECHOE "$fg$level:$PROGRAM:\033[0m$@"
}

log_info() {
    _log "INFO" "default" "$@"
}

log_warn() {
    _log "WARN" "red" "$@"
}

log_error() {
    _log "ERROR" "red" "$@"
}

install_packages() {
    local infile="$1"
    if [ -f "$infile" ]; then
        local pkgs=$( tr '\n' ' ' < $infile )
        echo "apk --quiet add $pkgs"
    else
        log_warn "Missing file $infile. No packages installed"
    fi
}


# vim: sw=4:et:ai
