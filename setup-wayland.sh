#!/bin/sh
# Wayland install script.
# It is supposed to be run independently, ie. from main.sh.

. bootstrap.sh

log_info "Start script"

[ "$USER" = "root" ] && exit 1;


log_info "Install packages"
install_packages packages-wayland.txt

log_info "Set up udev"
doas setup-udev

log_info "Set up services"
doas rc-update add seatd
doas rc-service seatd start
doas cp skel/xdg_runtime_dir.sh /etc/profile.d

log_info "Set up sway and user groups"
mkdir -p ~/.config/sway
cp /etc/sway/config ~/.config/sway

for g in video input seat; do doas adduser $USER $g; done

log_info "Done"

exit 0

# vim: sw=4:et:ai
